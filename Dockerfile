FROM node
COPY . /app
EXPOSE 3000
WORKDIR /app
RUN npm install
CMD npm start